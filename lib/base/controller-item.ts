// 基础控件的传值方式
// 一个组件对应多个字段的情况
// import rangeRef from './ref-model-range'
// 直接读写 model

// 不防抖
import modelRef from './ref/ref-model'
// 防抖
import modelDebounceRef from './ref/ref-model-debounce'
// 多字段
import modelRangeRef from './ref/ref-model-range'

// import { ref } from 'vue'
import type { Ref } from 'vue'

import type { IFormChildProps, IEventDebounce } from '../types/main'

/**
 * 表单子控件的控制函数
 * @param props 组件的 props，T 对应 model 的类型
 * @returns 返回一个 Ref<T>。
 */
export default function itemController<T extends object>(
  props: IFormChildProps<T>
) {

  // 从 props 里面获取 model 和属性
  const { model, meta } = props
  
  // 防抖的相关事件  
  const ctlEvents: IEventDebounce = {
    run: () => {}, // 立即执行，不防抖了
    clear: () => {} // 清除上一次计时，用于输入汉字的情况。
  }

  // 不防抖，立即提交
  const run = () => {
    ctlEvents.run() // 立即执行
  }
  // 输入汉字的情况
  const clear = (e: any) => {
    if (e.key !== 'Backspace') {
      ctlEvents.clear() // 输入汉字时清除上次计时
    }
  }

  // 父子组件传值的中转站，根据需求调用对应的函数。 
  let value: Ref<T> | Ref<T[keyof T]> | Ref<Array<T>> // | null

  // 判断是否多字段
  let isMoreColumn = false

  // colName 可以直接传入，也可以通过 meta 传入
  const colName = meta.colName

  let tmpArrayColName: Array<string> = [] // 拆分后的字段名称
  if (typeof colName === 'string') {
    tmpArrayColName = colName.split('_')
    // 需要支持多个字段
    isMoreColumn = (tmpArrayColName.length > 1)
  }

  // 判断是否需要多字段（无防抖）
  if (isMoreColumn && tmpArrayColName) {
    // 一个字段对应多个字段的情况，不防抖
    value = modelRangeRef(model, ...tmpArrayColName) 
  } else {
    // 判断防抖
    if (meta.delay && meta.delay > 0) {
      // 需要防抖
      value = modelDebounceRef(model, colName, ctlEvents, meta.delay)
    } else {
      // 不需要防抖
      value = modelRef(model, colName)
    }
  }

  // 设置默认值
  //if (meta.defValue) {
  //  value.value = meta.defValue
  // }

  return {
    value,
    run,
    clear
  }
}
