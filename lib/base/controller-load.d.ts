/**
 * 表单子控件的控制函数
 * @param props 组件的 props
 * @returns
 */
export default function loadController(props: any): {
    loadDict: () => void;
    loadDictById: (actionId: any, id: any) => Promise<unknown>;
};
