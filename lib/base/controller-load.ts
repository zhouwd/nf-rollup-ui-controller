
// 访问后端API
import service from '../service'
import type { IFormChildProps } from '../types/main'

/**
 * 表单子控件的控制函数
 * @param props 组件的 props
 * @returns 
 */
export default function loadController<T extends object> (props: IFormChildProps<T>) {
  // 属性里的事件集合，后端API参数，备选项
  const { webAPI, optionList } = props
  
  // 从后端API加载字典 =================================
  const loadDict = () => {
    if (webAPI?.serviceId) {
      // 设置了 serviceId ，需要获取后端API
      const url = `${webAPI.serviceId === '' ? '' : webAPI.serviceId}${webAPI.actionId === '' ? '' : webAPI.actionId}${webAPI.dataId === '' ? '' : webAPI.dataId}`
      service(url, '').then((res) => {
        // console.log('后端反馈 - ok', res)
        if (Array.isArray(res)) {
          if (optionList) {
            optionList.length = 0 
            optionList.push(...res)
          }
        }
      }).catch((err) => {
        console.error('后端反馈 - err', err)
      })
    }
  }

  // 从后端API加载字典 =================================
  const loadDictById = (actionId: any, id: any) => {
    return new Promise((resolve, reject) => {
      if (webAPI.serviceId !== null) {
        // 设置了 serviceId ，需要获取后端API
        const url = `${webAPI.serviceId === '' ? '' : webAPI.serviceId}/${actionId}/${id}`
        service(url, '').then((res) => {
          // console.log('后端反馈 - ok', res)
          if (Array.isArray(res)) {
            resolve(res)
          } else {
            reject({err: '不是数组'})
          }
        }).catch((err) => {
          reject(err)
        })
      }
    })
  }
  
  return {
    loadDict,
    loadDictById
  }
} 