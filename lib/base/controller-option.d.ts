declare const getChildrenOne: (data: any, parentId: any, level: any) => any;
declare const getChildren: (data: any, parentId: any, level: any) => any;
declare const shallowToDeep: (data: any) => {
    value: any;
    label: any;
    leaf: boolean;
    children: any[];
}[];
declare const cascaderManage: (data: any) => {
    propsCascader: {
        lazy: boolean;
        lazyLoad(node: any, resolve: any): void;
    };
};
export { getChildrenOne, getChildren, shallowToDeep, cascaderManage };
