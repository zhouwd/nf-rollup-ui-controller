import { ref, onMounted } from 'vue'

/**
 * 把组件的 返回去
 * @returns 
 */
export const myExpose = () => {
  // 获取 $ref
  const refControl = ref(null)
  // 容器
  const expose = {}
  // 渲染后绑定
  onMounted(() => {
    // console.log('_formControl', _formControl)
    Object.assign(expose, refControl.value)
  })

  return {
    refControl,
    expose
  }
}