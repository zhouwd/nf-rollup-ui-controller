import { customRef } from 'vue'

interface IModel {
  [key: string | number]: any
}

/**
 * 一个控件对应多个字段的情况，不支持 emit
 * @param model 表单的 model
 * @param arrColName 使用多个属性，数组
 */
export default function rangeRef<T extends object, K extends keyof T>
(
  model: T,
  ...arrColName: Array<K>
) {
  return customRef<Array<any>>((track: () => void, trigger: () => void) => {
    return {
      get(): Array<any> {
        track()
        // 多个字段，需要拼接属性值
        const tmp: Array<any> = []
        arrColName.forEach((col: K) => {
          // 获取 model 里面指定的属性值，组成数组的形式
          tmp.push(model[col])
        })
        return tmp
      },
      set (arrVal: Array<any>) {
        trigger()
        if (arrVal) {
          arrColName.forEach((col: K, i: number) => {
            // 拆分属性赋值，值的数量可能少于字段数量
            if (i < arrVal.length) {
              model[col] = arrVal[i]
            } else {
              model[col] = '' as T[K]
            }
          })
        } else {
          // 清空选择
          arrColName.forEach((col: K) => {
            model[col] = '' as T[K]
          })
        }
      }
    }
  })
}
 