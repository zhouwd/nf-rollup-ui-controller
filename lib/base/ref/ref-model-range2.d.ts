interface IModel {
    [key: string | number]: any;
}
/**
 * 一个控件对应多个字段的情况，不支持 emit
 * @param model 表单的 model
 * @param arrColName 使用多个属性，数组
 */
export default function range2Ref<T extends IModel, K extends keyof T>(model: T, ...arrColName: K[]): import("vue").Ref<any[]>;
export {};
