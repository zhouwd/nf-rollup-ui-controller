import type { IEventDebounce } from '../../types/20-form-item';
/**
 * 直接修改 model 的防抖
 * @param model 组件的 props 的 model
 * @param colName 需要使用的属性名称
 * @param events 事件集合，run：立即提交；clear：清空计时，用于汉字输入
 * @param delay 延迟时间，默认 500 毫秒
 */
export default function debounceRef<T, K extends keyof T>(model: T, colName: K, events: IEventDebounce, delay?: number): import("vue").Ref<T[K]>;
