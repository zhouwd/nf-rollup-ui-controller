import { customRef } from 'vue'

/**
 * 控件的直接输入，不需要防抖。负责父子组件交互表单值。
 * @param model 组件的 props 的 model
 * @param colName 需要使用的属性名称
 */
export default function modelRef<T, K extends keyof T>
(
  model: T,
  colName: K
) {
  
  /*
  return computed<T[K]>({
    get(): T[K] {
      // 返回 model 里面指定属性的值
      return model[colName]
    },
    set(val: T[K]) {
      // 给 model 里面指定属性赋值
      model[colName] = val
    }
  })
  */

  return customRef<T[K]>((track: () => void, trigger: () => void) => {
    return {
      get(): T[K] {
        track()
        // 返回 model 里面指定属性的值
        return model[colName]
      },
      set (val: T[K]) {
        trigger()
        // 给 model 里面指定属性赋值
        model[colName] = val
      }
    }
  })
}
