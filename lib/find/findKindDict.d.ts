/**
 * 查询方式
 * * 数字 =、<>、>=、<=、between
 * * 字符串 =、不等于、包含、起始于、结束于
 * * 日期时间 =、between
*/
export declare const findKindDict: {
    21: {
        id: number;
        name: string;
        where: string;
    };
    22: {
        id: number;
        name: string;
        where: string;
    };
    23: {
        id: number;
        name: string;
        where: string;
    };
    24: {
        id: number;
        name: string;
        where: string;
    };
    25: {
        id: number;
        name: string;
        where: string;
    };
    26: {
        id: number;
        name: string;
        where: string;
    };
    27: {
        id: number;
        name: string;
        where: string;
    };
    28: {
        id: number;
        name: string;
        where: string;
    };
    13: {
        id: number;
        name: string;
        where: string;
    };
    14: {
        id: number;
        name: string;
        where: string;
    };
    15: {
        id: number;
        name: string;
        where: string;
    };
    16: {
        id: number;
        name: string;
        where: string;
    };
    17: {
        id: number;
        name: string;
        where: string;
    };
    18: {
        id: number;
        name: string;
        where: string;
    };
    19: {
        id: number;
        name: string;
        where: string;
    };
    40: {
        id: number;
        name: string;
        where: string;
    };
    41: {
        id: number;
        name: string;
        where: string;
    };
    42: {
        id: number;
        name: string;
        where: string;
    };
};
