
import type { PropType } from 'vue'
type idType = string | number

import type { 
  IFromMeta // 表单子组件的属性
} from '../types/30-form'

import type {
  IQuery,
  ICusFind, // 自定义查询方式
  IQueryMini // 标准型查询：
} from '../types/40-find'

/**
 * 查询控件需要的属性 findProps
 */
export const findProps = {
  findMeta: {
    moduleId: [Number, String], // 模块ID
    quickFind: { // 快速查询的字段
      type: Array as PropType<Array<idType>>,
      default: () => {return []}
    },
    allFind: { // 全部查询的字段
      type: Array as PropType<Array<idType>>,
      default: () => {return []}
    },
    customer: { // 自定义的查询方案
      type: Array as PropType<Array<ICusFind>>,
      default: () => {return []}
    },
    customerDefault: { // 默认的查询方案
      type: [Number, String],
      default: 1
    },
    reload: { // 是否重新加载配置，需要来回取反
      type: Boolean,
      default: false
    }
  },
  queryMini: { // 用户输入的查询条件，紧凑型
    type: Object as PropType<IQueryMini>,
    default: () => { return []}
  },
  querys:{ // 用户输入的查询条件，标准型
    type: Array as PropType<Array<IQuery>>,
    default: () => { return {} }
  },
  itemMeta: { // 查询子控件的属性
    type: Object as PropType<IFromMeta>,
    default: () => { return {} }
  }
}
 