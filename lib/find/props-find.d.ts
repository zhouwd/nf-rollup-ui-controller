import type { PropType } from 'vue';
declare type idType = string | number;
/**
 * 查询控件需要的属性 findProps
 */
export declare const findProps: {
    findMeta: {
        moduleId: (StringConstructor | NumberConstructor)[];
        quickFind: {
            type: PropType<idType[]>;
            default: () => never[];
        };
        allFind: {
            type: PropType<idType[]>;
            default: () => never[];
        };
        customer: {
            type: PropType<ICusFind[]>;
            default: () => never[];
        };
        customerDefault: {
            type: (StringConstructor | NumberConstructor)[];
            default: number;
        };
        reload: {
            type: BooleanConstructor;
            default: boolean;
        };
    };
    queryMini: {
        type: PropType<IQueryMini>;
        default: () => never[];
    };
    querys: {
        type: PropType<IQuery[]>;
        default: () => {};
    };
    itemMeta: {
        type: PropType<IFromMeta>;
        default: () => {};
    };
};
export {};
