
import type { EControlType } from './enum'
type idType = string | number

export interface IAnyObject {
  [key: string | number]: any
}

/**
 * 访问后端API的配置
 * * serviceId：服务ID
 * * actionId
 * * dataId
 * * body
 * * cascader
 * * * lazy
 * * * actions
 * * * parentId
 */
export interface IWebAPI {
  /**
   * 服务ID/服务编号/服务名称
   */
  serviceId: idType,
  /**
   * 动作ID/动作名称
   */
  actionId: idType,
  /**
   * 主键字段值
   */
  dataId: idType,
  /**
   * form 的 body
   */
  body: any,
  /**
   * 联动组件需要的设置
   */
  cascader: {
    /**
     * 是否需要动态加载
     */
    lazy: boolean,
    /**
     * 按照level的顺序设置后端 API 的 action 名称
     */
    actions: Array<string>,
    /**
     * 每次提交时，父节点的ID值
     */
    parentId: string | number
  }
}

/**
 * 单层的选项，下拉列表等的选项。value、label
 */
export interface IOptionList {
  /**
   * 值，提交到后端，可以是数字或者文本
   */
  value: number | string,
  /**
   * 标签、文字说明
   */
  label: string,
  /**
   * 是否可用
   */
  disabled?: boolean
}

/**
 * 单层分组的选项，下拉列表等的选项。value、label
 */
export interface IOptionGrup {
  /**
   * 标签、文字说明
   */
  label: string,
  /**
   * 是否可用
   */
  options: IOptionList[]
}

/**
 * 多级的选项，级联控件。value、label、children（n级嵌套）
 */
export interface IOptionTree {
  /**
   * 值，提交到后端，可以是数字或者文本
   */
  value: idType,
  /**
   * 标签、文字说明
   */
  label: string,
  /**
   * 是否可用
   */
  disabled: boolean,
  /**
   * 子选项，可以无限嵌套
   */
  children: Array<IOptionTree>
}

/**
 * 防抖相关的事件
 */
export interface IEventDebounce {
  /**
   * 立即更新
   */
  run: () => void,
  /**
   * 清掉上一次的计时
   */
  clear: () => void
}

/**
 * 表单里 input 这类的 meta，
 * IFormItemMeta 更名： IFormChildMeta
 * * columnId：string | number， 字段ID、控件ID
 * * colName: string ，字段名称
 * * label?: string，字段的中文名称，标签
 * * labelWidth?: string | number，在表单里面，指定 label 的宽度
 * * controlType: EControlType | number，子控件类型 EControlType
 * * defValue?: any，子控件的默认值
 * * colCount?: number，一个控件占据的空间份数。
 * * delay?: number，防抖延迟时间，0：不延迟
 * * webapi?: IWebAPI，访问后端API的配置信息，有备选项的控件需要
 */
export interface IFormChildMeta {
  /**
   * 字段ID、控件ID
   */
  columnId: idType,
  /**
   * 字段名称
   */
  colName: string,
  /**
   * 字段的中文名称，标签
   */
  label?: string,
  /**
   * 在表单里面，指定 label 的宽度
   */
  labelWidth?: string | number,
  /**
   * 子控件类型，number，EControlType
   */
  controlType: EControlType | number,
  /**
   * 子控件的默认值
   */
  defValue?: any,
  /**
   * 一个控件占据的空间份数。
   * * ＜ 0 时，单列情况下有效：（多列情况下视为 1 ）
   * * * -2 占一半（12）
   * * * -3 占三分之一（8）
   * * * -4 占四分之一（6）
   * * * -6 占六分之一（4）
   * * ＞ 0 时，多列情况下有效：（单列情况下视为 1 ）
   * * * 1：占一份（最多占一行，下同）
   * * * 2：占两份
   * * * 3：占三份
   * * * 4：占四份
   * * * 6：占六份
   * * * 8：占八份
   * * * 12：占12份
   * * * 24：占24份
   */
  colCount?: -6 | -4 | -3 | -2 | 1 | 2 | 3 | 4 | 6 | 8 | 12 | 24,
  /**
   * 访问后端API的配置信息，有备选项的控件需要
   */
  webapi?: IWebAPI,
  /**
   * 防抖延迟时间，0：不延迟
   */
  delay?: number,
  /**
   * 防抖相关的事件
   */
  events?: IEventDebounce,
}

/**
 * 表单里 input 这类的 props，含 meta
 * IFormItemProps 更名： IFormChildProps
 * * meta: IFormChildMeta,input 这一类的需要的 meta
 * * model: T,单的 model，含义多个属性
 * * optionList?: Array<IOptionList | IOptionTree | IOptionGrup>,子控件备选项
 * * clearable?: boolean,是否显示清空的按钮
 */
export interface IFormChildProps<T extends IAnyObject> {
  /**
   * input 这一类的需要的 meta
   */
  meta: IFormChildMeta,
  /**
   * 表单的 model，含义多个属性
   */
  model: T,
  /**
   * 子控件备选项，一级或者多级
   */
  optionList?: Array<IOptionList | IOptionTree | IOptionGrup>,
  /**
   * 是否显示清空的按钮
   */
  clearable?: boolean,
  
   /**
   * 子控件的扩展属性
   */
  [key: string]: any
}


/**
 * 来自 json 的数据（itemMeta），key：meta + props 的结构，用于表单向 child 传递 参数
 * IFormItemList 更名： IFormChildPropsList
 * * 多个 key: string | number
 * * * meta: IFormChildMeta,
 * * * * columnId：string | number， 字段ID、控件ID
 * * * * colName: string ，字段名称
 * * * * label?: string，字段的中文名称，标签
 * * * * labelWidth?: string | number，在表单里面，指定 label 的宽度
 * * * * controlType: EControlType | number，子控件类型 EControlType
 * * * * defValue?: any，子控件的默认值
 * * * * colCount?: number，一个控件占据的空间份数。
 * * * * delay?: number，防抖延迟时间，0：不延迟
 * * * * webapi?: IWebAPI，访问后端API的配置信息，有备选项的控件需要
 * * * props: object input 的 props
 * * * * optionList 
 * * * * clearable 
 * * * * 其他扩展
 */
export interface IFormChildPropsList {
  /**
   * 表单控件里的子控件的 meta
   */
  [key: string | number]: {
    meta: IFormChildMeta,
    props: object
  }
}

/**
 * 只有 input 的 props 的集合
 */
export interface IFormChildOnlyPropsList {
  [key: string | number]: object
}

/**
 * 表单里 input 这类的 meta 集合（纯），用于表单内部传递 meta 
 * IFormItemMetaList 更名： IFormChildMetaList
 * * 多个 key：number。value：IFormChildMeta
 * * * columnId：string | number， 字段ID、控件ID
 * * * colName: string ，字段名称
 * * * label?: string，字段的中文名称，标签
 * * * labelWidth?: string | number，在表单里面，指定 label 的宽度
 * * * controlType: EControlType | number，子控件类型 EControlType
 * * * defValue?: any，子控件的默认值
 * * * colCount?: number，一个控件占据的空间份数。
 * * * delay?: number，防抖延迟时间，0：不延迟
 * * * webapi?: IWebAPI，访问后端API的配置信息，有备选项的控件需要
 */
export interface IFormChildMetaList  {
  /**
   * 表单控件里的子控件的 meta
   */
  [key: string | number]: IFormChildMeta 
}
 