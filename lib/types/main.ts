
export type {
  IAnyObject, // 任意对象
  IWebAPI, // 访问后端API的配置
  IOptionList, // 单层的选项，下拉列表等的选项。value、label
  IOptionTree, // 多级的选项，级联控件。value、label、children（n级嵌套）
  IEventDebounce, // 防抖相关的事件
  IFormChildPropsList, // 表单里 input 这类的 props 集合，含 meta
  IFormChildMetaList, // 表单里 input 这类的 meta 集合（纯）
  IFormChildOnlyPropsList, // 纯 input 的 props
  IFormChildMeta, // 表单里 input 这类的 meta
  IFormChildProps // 表单里 input 这类的 props，含meta
} from './20-form-child'

export type {
  FormColSpan,
  ShowCol,
  IFormState,
  ILinkageMeta, // 显示控件的联动设置
  IRule, // 一条验证规则，一个控件可以有多条验证规则
  IRuleMeta, // 表单的验证规则集合
  ISubMeta, // 分栏表单的设置
  IFromMeta, // 低代码的表单需要的 meta
  IFromProps // 表单控件的属性，约束必须有的属性

} from './30-form'

export type {
  IFormItemMeta
} from './35-form-item'

export type {
  ICusFind, // 自定义查询方式
  IQueryMini, // 紧凑型查询：IFindArray
  IQuery, // 标准型查询
  IFindProps, // 查询控件的属性
  IFindState // 查询需要的状态，便于各个组件传递数据
} from './40-find'

export type {
  IGridMeta, // 列表的 meta
  IGridItem, // 列的属性
  IGridItemList, // 列表字段的列的属性
  IGridSelection, // 列表里选择的数据
  IGridProps // 列表控件的属性的类型

} from './50-grid'

export type {
  IDragInfo, // 拖拽时记录相关信息
  IDragEvent // 拖拽相关的事件
} from './70-drag'

export type {
  IMenuList, // 多层的菜单
  IMenu, // 一个菜单的类型
  IMenus, // 创建路由-菜单时的对象类型
  ICurrentRoute, // 
  // IProps, // 泛型的约束
  IRouter, // 路由的类型

} from './90-router'

export {
  EControlType, // 控件类型的枚举
  EFindKind, // 查询方式的枚举
  EAlign, // 横向对齐方式，左、中、右
  ESize, //  控件尺寸，大中小
  ESubType // 表单控件的分栏类型。
} from './enum'