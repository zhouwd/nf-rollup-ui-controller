/// <reference types="node" />
/**
 * 拖拽时记录相关信息
 */
export interface IDragInfo {
    /**
     * 拖拽移除的延迟
     */
    timeout: NodeJS.Timeout;
    oldDom: {
        style: {
            border: string;
            borderStyle: string;
        };
    };
    /**
     * 状态
     */
    state: string;
    /**
     * 拖拽时X坐标
     */
    offsetX: number;
    /**
     * 是否左面释放鼠标
     */
    isLeft: boolean;
    /**
     * 是否按下 ctrl
     */
    ctrl: boolean;
    /**
     * 开始的id
     */
    targetId: string;
    /**
     * 开始的表头，判断是否拖拽出去
     */
    targetLabel: string;
    /**
     * 开始的 target
     */
    target: any;
    /**
     * 结束的ID
     */
    sourceId: string;
    /**
     * 开始的序号
     */
    targetIndex: number;
    /**
     * 结束的序号
     */
    sourceIndex: number;
}
/**
 * 拖拽相关的事件
 */
export interface IDragEvent {
    /**
     * 设置对齐方式的函数
     */
    setAlign: () => void;
    /**
     * 设置排序
     */
    setOrder: () => void;
    /**
     * 移除字段
     */
    removeDom: () => void;
    /**
     * ctrl + 单击控件（字段）触发的事件，可以用于打开修改界面
     */
    modCol: (colId: string | number, event: any) => void;
}
