
import config from '../config/index.js'

export default function webAPI(url, query = '', body = null) {

  return new Promise((resolve, reject) => {
    const _url = `${config.baseUrl === '/' ? '/':config.baseUrl}${url}${(query)? '?' + query : ''}`
    if (body === null) {
      config.axios.get(_url)
        .then((res) => {
          // 后端API返回数据
          resolve(res.data)
        })
        .catch((err) => {
          reject(err)
        })
    } else {
      config.axios.post(_url, body)
        .then((res) => {
          // 后端API返回数据
          resolve(res.data)
        })
        .catch((err) => {
          reject(err)
        })
    }
  })

}