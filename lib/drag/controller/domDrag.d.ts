import type { IDragInfo } from '../../types/70-drag';
/**
 * 把 dom 变成可以拖拽的形式，记录拖拽信息。
 * * 针对table的th，form 和 find 的div
 */
export default class domDrag {
    dom: any;
    align: () => void;
    order: () => void;
    removeDom: () => void;
    modCol: (colId: string | number, event: any) => void;
    /**
     * 传入一个 dom，设置拖拽属性，触发拖拽事件
     * @param _dom 要拖拽的dom，比如 table 的th，表单的div等
     * @param colOrder 排序的字段ID  Array<number|string>
     * @param dragInfo 拖拽信息 IDragInfo
     * @param colId 字段ID string | number
     * @param dragEvent 拖拽触发的事件 IDragEvent
     */
    constructor(_dom: HTMLTableCellElement | HTMLDivElement | any, //需要拖拽的dom
    colOrder: Array<number | string>, //用于字段的排序
    dragInfo: IDragInfo, //拖拽信息
    colId: string, //字段ID
    dragEvent: any);
}
