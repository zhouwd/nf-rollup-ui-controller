import type { IFormItemMeta } from '../types/20-form-item';
/**
 * @function 创建列表的 dataList，演示用
 * @description 依据表单的 itemMeta，创建 演示用数据
 * @param meta 表单控件的 meta
 */
export default function createDataList(meta: IFormItemMeta, count?: number): never[];
