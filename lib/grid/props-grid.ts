import type { PropType } from 'vue'

import type {
  IGridMeta,
  IGridItem,
  IGridSelection
} from '../types/50-grid'

/**
 * 表单控件需要的属性propsForm
 */
export const gridProps = {
  /* 列表的 meta，LowCode 需要。
   * * moduleId: number | string,
   * * idName: String,
   * * colOrder: Array<number|string>
   */
  gridMeta: {
    type: Object as PropType<IGridMeta>
  },
  /**
   * 高度
   */
  height: {
    type: [Number, String],
    default: 500
  },
  /**
   * 斑马纹
   */
  stripe: {
    type: Boolean,
    default: true
  },
  /**
   * 纵向边框
   */
  border: {
    type: Boolean,
    default: true
  },
  /**
   * 列的宽度是否自撑开
   */
  fit: {
    type: Boolean,
    default: true
  },
   /**
   * 要高亮当前行，Boolean
   */
  highlightCurrentRow: {
    type: Boolean,
    default: true
  },
  /**
   * table的列的 meta
   */
  itemMeta: {
    type: Object as PropType<{
      [key:string | number]: IGridItem
    }>
  },
  /**
   * 选择的情况 IGridSelection
   */
  selection: {
    type: Object as PropType<IGridSelection>,
    default: () => {
      return {
        dataId: '', // 单选ID number 、string
        row: {}, // 单选的数据对象 {}
        dataIds: [], // 多选ID []
        rows: [] // 多选的数据对象 []
      }
    }
  },
  /**
   * 绑定的数据
   */
  dataList: {
    type: Array as PropType<Array<any>>,
    default: () => []
  }
}
