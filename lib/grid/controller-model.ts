
import { reactive } from 'vue'

import type {
  IFormChildPropsList
} from '../types/20-form-child'

/**
 * @function 创建列表的 dataList，演示用
 * @description 依据表单的 itemMeta，创建 演示用数据
 * @param meta 表单控件的 meta
 */
export default function createDataList(meta: IFormChildPropsList, count = 10) {
  const list = reactive([])

  const create = (flag) => {
    // 依据 meta，创建 model
    const list = {}

    for(const key in meta) {
      const m = meta[key].meta
      // 根据控件类型设置属性值
      switch (m.controlType) {
        case 100: // 文本类
        case 101:
        case 102:
        case 103:
        case 104:
        case 105:
        case 106:
        case 107:
        case 108:
          list[m.colName] = '文本' + flag
          break
        case 120: // 日期
          list[m.colName] = '2021-10-10'
          break
        case 121: // 日期时间
          list[m.colName] = '2021-10-10 12:06:66'
          break
        case 122: // 年月
          list[m.colName] = '2021-10'
          break
        case 124: // 年
          list[m.colName] = '2021年'
          break
        case 123: // 年周
          list[m.colName] = '2021-w23'
          break
        case 125: // 日期范围
          list[m.colName] = ['2021-10-10', '2022-01-01']
          break
        case 126: // 日期时间范围
          list[m.colName] = ['2021-10-10 10:22:22', '2022-01-01 14:33:33']
          break
        case 127: // 年月范围
          list[m.colName] = ['2021-12', '2022-02']
          break
        case 128: // 日期多选
          list[m.colName] = ['2021-10-10', '2022-01-01']
          break
        case 130: // 任意时间
          list[m.colName] = '12:02:06'
          break
        case 131: // 选择时间
          list[m.colName] = '9:30'
          break
        case 110: // 数字
        case 111:
          list[m.colName] = 100 + flag * 1
          break
        case 112:
          list[m.colName] = 3
          break
        case 150: // 勾选
        case 151: // 开关
          list[m.colName] = false
          break
        case 153: // 单选组
        case 160: // 下拉单选
          list[m.colName] = 2
          break
        case 164: // 下拉联动
          // list[m.colName] = []
          m.colName.split('_').forEach((col: string) => {
            list[col] = flag
          })
          break
        case 152: // 多选组
        case 161: // 下拉多选
          list[m.colName] = [8, flag]
          break
      }

      // 看看有没有设置默认值
      if (typeof m.defValue !== 'undefined') {
        switch (m.defValue) {
          case '':
            break
          case '{}':
            // list[m.colName] = {}
            break
          case '[]':
            // list[m.colName] = []
            break
          case '{{now}}':
            list[m.colName] = new Date()
            break
          default:
            if (Array.isArray(m.defValue)) {
              if (m.defValue.length > 0) {
                list[m.colName] = m.defValue
              }
            } else {
              list[m.colName] = m.defValue
            }
            break
        }
      }
    }

    if (typeof list.ID === 'undefined') {
      list.ID = flag
    }
    return list
  }

  for (let i = 0; i < count; i++) {
    list.push(create(i))
  }

  return list
}
