import serviceConfig from './config';
import service from './service';
import lifecycle from './base/lifecycle';
import { createRouter, useRouter } from './router/router';
import { itemProps } from './base/props-item';
import { formProps } from './form/props-form';
import { findProps } from './find/props-find';
import { gridProps } from './grid/props-grid';
import { getChildrenOne, getChildren, shallowToDeep, cascaderManage } from './base/controller-option';
import custmerRef from './base/ref/ref-model';
import debounceRef from './base/ref/ref-emit-debounce';
import emitRef from './base/ref/ref-emit';
import emitDebRef from './base/ref/ref-emit-debounce';
import modelRef from './base/ref/ref-model';
import rangeRef from './base/ref/ref-model-range';
import range2Ref from './base/ref/ref-model-range2';
import modelDebRef from './base/ref/ref-model-debounce';
import loadController from './base/controller-load';
import itemController from './base/controller-item';
import { createModel } from './form/controllar/createModel';
import formController from './form/controller-form';
import { findKindDict } from './find/findKindDict';
import createDataList from './grid/controller-model';
import mykeydown from './key/keydown';
import mykeypager from './key/keypager';
import dialogDrag from './drag/dialogDrag';
import gridDrag from './drag/gridDrag';
import formDrag from './drag/formDrag';
import _domDrag from './drag/controller/domDrag';
declare const domDrag: (th: any, meta: any, dragInfo: any, colId: any) => _domDrag;
export { serviceConfig, service, lifecycle, createRouter, useRouter, itemProps, // 表单子控件的共用属性
formProps, // 表单控件的属性
findProps, // 表单控件的属性
gridProps, // 列表控件的属性
getChildrenOne, getChildren, shallowToDeep, cascaderManage, // 下拉列表框等的选项的控制类
createModel, // 创建表单需要的 v-model
loadController, // 加载后端字典
itemController, // 表单子控件的控制类
formController, // 表单控件的控制类
findKindDict, // 查询方式的字典
createDataList, // 建立演示数据
mykeydown, // 操作按钮
mykeypager, // 翻页的按键
dialogDrag, // 拖拽 dialog
gridDrag, // 数据列表的拖拽设置
formDrag, // 设置表单的拖拽
domDrag, // 设置 td、div可以拖拽
emitRef, emitDebRef, modelRef, rangeRef, range2Ref, modelDebRef, debounceRef, // 防抖
custmerRef };
