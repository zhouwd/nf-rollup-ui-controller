
/**
 * 设置联动筛选需要的信息
 */

import { watch } from 'vue'

// 创建局部 model
import setPartModel from './setPartModel'
 
// 表单控件需要的 meta
import type {
  IAnyObject,
  IFormState
} from '../../main'

/**
 * 设置备选项和子控件的联动
 * * 改为，从 state 里面统一获取内容，
 * @param state 表单控件的 状态
 * @returns 
 */
export default function setControlShow<T extends IAnyObject>(
  state: IFormState<T>
) {
  
  const {
    model,
    partModel,
    showCol,
    childMetaList,
    linkageMeta
  } = state
  
  // 依据meta设置，默认都可见
  const setShow = () => {
    if (typeof childMetaList === 'object') {
      for (const key in childMetaList) {
        showCol[key] = true
      }
    }
  }

  const setValue = (modelValue: any, mainComp: any ) => {
    switch (typeof modelValue){
      case 'number':
      case 'string':
        // 单选组的选项，先让字段都不可见，然后依据配置信息设置字段可见。
        Object.keys(childMetaList).forEach(key => {
          showCol[key] = false
        })
        // 配置信息里对应的字段ID集合
        mainComp[modelValue].forEach((id: string) => {
          showCol[id] = true
        })
        break;
      case 'boolean':
        // 设置指定的控件是否可见，其他控件不设置
        mainComp[modelValue.toString()].forEach((id: any) => {
          let _id = id
          let bl = modelValue
          if (Array.isArray(id)) {
            _id = id[0]
            // 设置子控件的值
            const colName = childMetaList[_id].colName
            model[colName] = id[1]
          }
          // 字符串形式的ID，取反
          if (typeof _id === 'string' ) {
            bl = !modelValue
          }
          showCol[_id] = bl // 以 父级控件的值为准
        })
        break
    }
  }

  // 设置联动
  const setFormColShow = () => {
    setShow()
    // 数据变化，联动组件的显示
    if (typeof linkageMeta !== 'object') {
      // 配置信息不是对象，退出
      return
    }
    if (Object.keys(linkageMeta).length === 0) {
      // 配置信息没有内容，退出
      return
    }

    for (const key in linkageMeta) {
      // 配置里面设置的主动组件。
      const mainComp = linkageMeta[key]
      // 主动组件的字段名称
      const colName = childMetaList[key].colName
      // 监听组件的值，有变化就重新设置局部 model
      if (typeof model[colName] !== 'undefined') {
        // 监听主动组件的值的变化
        watch(() => model[colName], (modelValue, oldValue) => {
          if (modelValue === null) {
            // 选项被清空了，设为全部可见
            Object.keys(childMetaList).forEach(key => {
              showCol[key] = true
            })
            return
          } 

          if (typeof mainComp[modelValue] === 'undefined') {
            // 配置信息里面，没有设置选项值 需要显示的字段。
            return
          }
          // 设置联动
          setValue(modelValue, mainComp)
          
          // 设置部分的 model
          setPartModel<T>(state)
        },
        { immediate: true })
      }
    }
    // 监听完整model的值的变化，同步值
    if (typeof partModel == 'object') {
      watch(model, () => {
        for (const key in model) {
          if (typeof(partModel[key]) !== 'undefined') {
            partModel[key] = model[key]
          }
        }
      })
    }
  }

  setFormColShow()

  // watch(() => colOrder, (order) => {
  //  order.forEach((id: string | number) => {
  //    showCol[id] = true
  //  })
    // setFormColShow()
  // })
  
  return {
    setFormColShow
  }
}