declare const _default: import("vue").DefineComponent<{
    model: import("../../../lib/types/10-base-validation").IPropsValidation;
    partModel?: import("../../../lib/types/10-base-validation").IPropsValidation | undefined;
    colOrder: import("../../../lib/types/10-base-validation").IPropsValidation;
    formColCount: import("../../../lib/types/10-base-validation").IPropsValidation;
    labelSuffix: import("../../../lib/types/10-base-validation").IPropsValidation;
    labelWidth: import("../../../lib/types/10-base-validation").IPropsValidation;
    size: import("../../../lib/types/10-base-validation").IPropsValidation;
    itemMeta: import("../../../lib/types/10-base-validation").IPropsValidation;
    subs: import("../../../lib/types/10-base-validation").IPropsValidation;
    ruleMeta: import("../../../lib/types/10-base-validation").IPropsValidation;
    formColShow: import("../../../lib/types/10-base-validation").IPropsValidation;
    customerControl: import("../../../lib/types/10-base-validation").IPropsValidation;
    reload: import("../../../lib/types/10-base-validation").IPropsValidation;
    reset: import("../../../lib/types/10-base-validation").IPropsValidation;
    events: import("../../../lib/types/10-base-validation").IPropsValidation;
}, {
    props: Readonly<import("@vue/shared").LooseRequired<Readonly<import("vue").ExtractPropTypes<{
        model: import("../../../lib/types/10-base-validation").IPropsValidation;
        partModel?: import("../../../lib/types/10-base-validation").IPropsValidation | undefined;
        colOrder: import("../../../lib/types/10-base-validation").IPropsValidation;
        formColCount: import("../../../lib/types/10-base-validation").IPropsValidation;
        labelSuffix: import("../../../lib/types/10-base-validation").IPropsValidation;
        labelWidth: import("../../../lib/types/10-base-validation").IPropsValidation;
        size: import("../../../lib/types/10-base-validation").IPropsValidation;
        itemMeta: import("../../../lib/types/10-base-validation").IPropsValidation;
        subs: import("../../../lib/types/10-base-validation").IPropsValidation;
        ruleMeta: import("../../../lib/types/10-base-validation").IPropsValidation;
        formColShow: import("../../../lib/types/10-base-validation").IPropsValidation;
        customerControl: import("../../../lib/types/10-base-validation").IPropsValidation;
        reload: import("../../../lib/types/10-base-validation").IPropsValidation;
        reset: import("../../../lib/types/10-base-validation").IPropsValidation;
        events: import("../../../lib/types/10-base-validation").IPropsValidation;
    }>> & {
        [x: string & `on${string}`]: ((...args: any[]) => any) | ((...args: unknown[]) => any) | undefined;
    }>>;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    model: import("../../../lib/types/10-base-validation").IPropsValidation;
    partModel?: import("../../../lib/types/10-base-validation").IPropsValidation | undefined;
    colOrder: import("../../../lib/types/10-base-validation").IPropsValidation;
    formColCount: import("../../../lib/types/10-base-validation").IPropsValidation;
    labelSuffix: import("../../../lib/types/10-base-validation").IPropsValidation;
    labelWidth: import("../../../lib/types/10-base-validation").IPropsValidation;
    size: import("../../../lib/types/10-base-validation").IPropsValidation;
    itemMeta: import("../../../lib/types/10-base-validation").IPropsValidation;
    subs: import("../../../lib/types/10-base-validation").IPropsValidation;
    ruleMeta: import("../../../lib/types/10-base-validation").IPropsValidation;
    formColShow: import("../../../lib/types/10-base-validation").IPropsValidation;
    customerControl: import("../../../lib/types/10-base-validation").IPropsValidation;
    reload: import("../../../lib/types/10-base-validation").IPropsValidation;
    reset: import("../../../lib/types/10-base-validation").IPropsValidation;
    events: import("../../../lib/types/10-base-validation").IPropsValidation;
}>>, {
    size: any;
    model: any;
    colOrder: any;
    itemMeta: any;
    reload: any;
    reset: any;
    events: any;
    formColCount: any;
    labelSuffix: any;
    labelWidth: any;
    subs: any;
    ruleMeta: any;
    formColShow: any;
    customerControl: any;
}>;
export default _default;
