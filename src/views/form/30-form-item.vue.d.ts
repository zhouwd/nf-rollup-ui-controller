declare const _default: import("vue").DefineComponent<{
    columnId: import("../../../lib/types/10-base-validation").IPropsValidation;
    model: import("../../../lib/types/10-base-validation").IPropsValidation;
    colName: import("../../../lib/types/10-base-validation").IPropsValidation;
    controlType: import("../../../lib/types/10-base-validation").IPropsValidation;
    optionList: import("../../../lib/types/10-base-validation").IPropsValidation;
    webAPI: import("../../../lib/types/10-base-validation").IPropsValidation;
    delay: import("../../../lib/types/10-base-validation").IPropsValidation;
    events: import("../../../lib/types/10-base-validation").IPropsValidation;
    size: import("../../../lib/types/10-base-validation").IPropsValidation;
    clearable: import("../../../lib/types/10-base-validation").IPropsValidation;
    extend: import("../../../lib/types/10-base-validation").IPropsValidation;
    aa: StringConstructor;
}, {
    props: Readonly<import("@vue/shared").LooseRequired<Readonly<import("vue").ExtractPropTypes<{
        columnId: import("../../../lib/types/10-base-validation").IPropsValidation;
        model: import("../../../lib/types/10-base-validation").IPropsValidation;
        colName: import("../../../lib/types/10-base-validation").IPropsValidation;
        controlType: import("../../../lib/types/10-base-validation").IPropsValidation;
        optionList: import("../../../lib/types/10-base-validation").IPropsValidation;
        webAPI: import("../../../lib/types/10-base-validation").IPropsValidation;
        delay: import("../../../lib/types/10-base-validation").IPropsValidation;
        events: import("../../../lib/types/10-base-validation").IPropsValidation;
        size: import("../../../lib/types/10-base-validation").IPropsValidation;
        clearable: import("../../../lib/types/10-base-validation").IPropsValidation;
        extend: import("../../../lib/types/10-base-validation").IPropsValidation;
        aa: StringConstructor;
    }>> & {
        [x: string & `on${string}`]: ((...args: any[]) => any) | ((...args: unknown[]) => any) | undefined;
    }>>;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    columnId: import("../../../lib/types/10-base-validation").IPropsValidation;
    model: import("../../../lib/types/10-base-validation").IPropsValidation;
    colName: import("../../../lib/types/10-base-validation").IPropsValidation;
    controlType: import("../../../lib/types/10-base-validation").IPropsValidation;
    optionList: import("../../../lib/types/10-base-validation").IPropsValidation;
    webAPI: import("../../../lib/types/10-base-validation").IPropsValidation;
    delay: import("../../../lib/types/10-base-validation").IPropsValidation;
    events: import("../../../lib/types/10-base-validation").IPropsValidation;
    size: import("../../../lib/types/10-base-validation").IPropsValidation;
    clearable: import("../../../lib/types/10-base-validation").IPropsValidation;
    extend: import("../../../lib/types/10-base-validation").IPropsValidation;
    aa: StringConstructor;
}>>, {
    size: any;
    model: any;
    colName: any;
    clearable: any;
    optionList: any;
    columnId: any;
    controlType: any;
    webAPI: any;
    delay: any;
    events: any;
    extend: any;
}>;
export default _default;
