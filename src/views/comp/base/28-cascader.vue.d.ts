import type { PropType } from 'vue';
interface DateRange {
    d1: string;
    d2: string;
    d3: string;
}
declare const _default: import("vue").DefineComponent<{
    model: {
        type: PropType<DateRange>;
        default: () => {
            name: string;
        };
    };
    colName: {
        type: StringConstructor[];
        default: string;
    };
}, {
    cprops: {
        expandTrigger: string;
    };
    handleChange: (value: Array<any>) => void;
    options: ({
        value: string;
        label: string;
        children: {
            value: string;
            label: string;
            children: {
                value: string;
                label: string;
            }[];
        }[];
    } | {
        value: string;
        label: string;
        children: {
            value: string;
            label: string;
        }[];
    })[];
    val2: import("vue").Ref<(keyof DateRange)[]>;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    model: {
        type: PropType<DateRange>;
        default: () => {
            name: string;
        };
    };
    colName: {
        type: StringConstructor[];
        default: string;
    };
}>>, {
    model: DateRange;
    colName: string;
}>;
export default _default;
