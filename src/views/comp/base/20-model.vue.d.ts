import type { PropType } from 'vue';
interface Person {
    name: string;
}
declare const _default: import("vue").DefineComponent<{
    model: {
        type: PropType<Person>;
        default: () => {
            name: string;
        };
    };
    colName: {
        type: StringConstructor;
        default: string;
    };
}, {
    val2: import("vue").Ref<unknown>;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    model: {
        type: PropType<Person>;
        default: () => {
            name: string;
        };
    };
    colName: {
        type: StringConstructor;
        default: string;
    };
}>>, {
    model: Person;
    colName: string;
}>;
export default _default;
