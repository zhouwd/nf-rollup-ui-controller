
import { reactive } from 'vue'

/**
 * 设置表单需要用的子控件
 */
export const config = {
  formItem: reactive({})
}