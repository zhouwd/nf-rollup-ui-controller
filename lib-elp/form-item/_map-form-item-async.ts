import { defineAsyncComponent } from 'vue'

// 文本
// import nfArea from './t-area.vue' // 100

const nfArea = defineAsyncComponent(() => import('./t-area.vue'))
const nfText = defineAsyncComponent(() => import('./t-text.vue'))
const nfPassword = defineAsyncComponent(() => import('./t-password.vue'))
const nfUrl = defineAsyncComponent(() => import('./t-url.vue'))
const nfAutocomplete = defineAsyncComponent(() => import('./t-autocomplete.vue'))
const nfColor = defineAsyncComponent(() => import('./t-color.vue'))

// 数字
const nfNumber = defineAsyncComponent(() => import('./n-number.vue'))
const nfRange = defineAsyncComponent(() => import('./n-range.vue'))
const nfRate = defineAsyncComponent(() => import('./n-rate.vue'))

// 日期
const nfDate = defineAsyncComponent(() => import('./d-date-picker.vue'))
const nfWeek = defineAsyncComponent(() => import('./d-week.vue'))

// 时间
const nfTimePicker = defineAsyncComponent(() => import('./d-time-picker.vue'))
const nfTimeSelect = defineAsyncComponent(() => import('./d-time-select.vue'))
// 二选一
const nfSwitch = defineAsyncComponent(() => import('./s1-switch.vue'))
const nfCheckbox = defineAsyncComponent(() => import('./s1-checkbox.vue'))
// 选择
const nfCheckboxs = defineAsyncComponent(() => import('./s2-checkboxs.vue'))
const nfRadios = defineAsyncComponent(() => import('./s2-radios.vue'))
// 下拉
const nfSelect = defineAsyncComponent(() => import('./s-select.vue'))
const nfSelectGroup = defineAsyncComponent(() => import('./s-select-group.vue'))
const nfSelectCascader = defineAsyncComponent(() => import('./s-select-cascader.vue'))
const nfSelectTree = defineAsyncComponent(() => import('./s-select-tree.vue'))
// 上传文件
const nfFile = defineAsyncComponent(() => import('./u-file.vue'))
const nfPicture = defineAsyncComponent(() => import('./u-picture.vue'))
const nfVideo = defineAsyncComponent(() => import('./u-video.vue'))

/*
import nfText from './t-text.vue' // 101
import nfPassword from './t-password.vue' // 102
import nfUrl from './t-url.vue' // 105
import nfAutocomplete from './t-autocomplete.vue' // 107
import nfColor from './t-color.vue' // 108
// 数字
import nfNumber from './n-number.vue' // 120
import nfRange from './n-range.vue' // 121
import nfRate from './n-rate.vue' // 122
// 日期
import nfDate from './d-date-picker.vue'
import nfWeek from './d-week.vue' // 123

// 时间
import nfTimePicker from './d-time-picker.vue' // 130
import nfTimeSelect from './d-time-select.vue' // 131

// 二选一
import nfSwitch from './s1-switch.vue' // 150
import nfCheckbox from './s1-checkbox.vue' // 151
// 选择
import nfCheckboxs from './s2-checkboxs.vue' // 152
import nfRadios from './s2-radios.vue' // 153
// 下拉
import nfSelect from './s-select.vue' // 160 单选
import nfSelectGroup from './s-select-group.vue' // 162 分组
import nfSelectCascader from './s-select-cascader.vue' // 164 级联
import nfSelectTree from './s-select-tree.vue' // 165 单选

// 上传文件
import nfFile from './u-file.vue'
// 上传图片
import nfPicture from './u-picture.vue'
// 上传视频
import nfVideo from './u-video.vue'

*/

// import { config } from '../../config'

// 各种选项
// import nfOption from './o-option.vue' // 普通选项
// import nfOptionGroup from './o-option-group.vue' // 分组选项
// import nfOptionTree from './o-option-tree.vue' // 分组选项

/**
 * 文本类：文本 单行 多行 URL 密码 颜色 可选可填
 */
const textComp = {
  nfText,  nfArea,  nfUrl,  nfPassword,  nfColor,  nfAutocomplete
}

/**
 * 数字类：数字 滑块 评分
 */
const numComp = {
  nfNumber,  nfRange,  nfRate
}

/**
 * 日期综合  年周
 */
const dateComp = {
  nfDate,  nfWeek
}

/**
 * 时间
 */
const timeComp = {
  nfTimePicker,  nfTimeSelect
}

/**
 * 上传文件 图片 视频
 */
const uploadComp = {
  nfFile,  nfPicture,  nfVideo
}

/**
 * 开关  勾选 多选组 单选组
 */
const switchComp = {
  nfSwitch,  nfCheckbox,  nfCheckboxs,  nfRadios
}

/**
 * 下拉
 */
const selectComp = {
  nfSelect, // 160 单选
  // // nfSelects, // 161 多选
  nfSelectGroup, // 162 分组=
  nfSelectCascader, // 164 联动
  nfSelectTree // 165 树
  // // nfSelectFind, // 远程查询

  // 选项 分组选项 树状选项
  // nfOption,  nfOptionGroup,  nfOptionTree
}

const AllFormItem = {
  // 文本 单行 多行 URL 密码 颜色 可选可填
  ...textComp,
  // 数字 滑块 评分
  ...numComp,
  // 日期综合  年周
  ...dateComp, 
  // 时间
  ...timeComp,
  // 上传文件 图片 视频
  ...uploadComp,
  // 开关  勾选 多选组 单选组
  ...switchComp,
  // 下拉
  ...selectComp
}


const formItemKey = {
  
}

const formItem = {

}

export {
  formItemKey,
  AllFormItem,
  formItem
}