
/**
 * 查询方式的枚举
 */
export const enum EFindKind {
  // 字符串
  strEqual = 401, // '{col} = ?'
  strNotEqual = 402, // '{col} <> ?'
  strInclude = 403, // '{col} like "%?%"'
  strNotInclude = 404, // '{col} not like "%?%"'
  strStart = 405, // '{col} like "?%"'
  strEnd = 406, // '{col} like "%?"'
  strNotStart = 407, // '{col} not like "?%"'
  strNotEnd = 408, // '{col} not like "%?"'
  // 数字、日期时间
  numEqual = 411, // '{col} = ?'
  numNotEqual = 412, // '{col} <> ?'
  numInclude = 413, // '{col} > ?'
  numNotInclude = 414, // '{col} >= ?'
  numStart = 415, // '{col} < ?'
  numEnd = 416, // '{col} <= ?'
  numBetween = 417, // '{col} between ? and ?'
  numBetweenEnd = 418, // '{col} > ? and {col} <= ?'
  numBetweenStart = 419, // '{col} >= ? and {col} < ?'
  // 多选
  rangeInclude = 440, // '( {col} like %?% or {col} like %?% ... ) '
  rangeIn = 441, // '{col} in (?)'
  rangeNotIn = 442 // '{col} not in (?)'
  
}

/**
 * 横向对齐方式，左、中、右
 */
export const enum EAlign {
  left = 'left',
  center = 'center',
  right = 'right'
}

/**
 * 控件尺寸，大中小
 */
export const enum ESize {
  big = 'large',
  def = 'default',
  small = 'small'
}

/**
 * 表单控件的分栏类型。
 * * card：卡片
 * * tab：多标签
 * * step：分步
 * * no：不分栏
 */
export const enum ESubType {
  card = 'card',
  tab = 'tab',
  step = 'step',
  no = ''
}
