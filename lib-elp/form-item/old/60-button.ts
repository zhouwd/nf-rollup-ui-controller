
// 按钮组控件 ===========================================================

type idType = string | number

/**
 * 操作按钮的信息
 */
export interface IButtonMeta {
  /**
   * 按钮标题
   */
  btnTitle: string,
  /**
   * 按钮类型
   */
  btnKind: 'add' | 'update' | 'del',
  /**
   * 弹窗的标题
   */
  dialogTitle: string,
  /**
   * 弹窗的宽度
   */
  dialogWidth: string,
  /**
   * 打开的窗口类型：表单、列表、自定义
   */
  controlKey: 'form' | 'list' | string,
  /**
   * 打开的窗口的模块ID
   */
  openModuleId: idType,
  /**
   * 表单的ID
   */
  formMetaId: idType,
  /**
   * 提交到后端的 “动作ID”
   */
  actionId: idType,
  /**
   * 热键
   */
  hotkey: string
}

/**
 * 单击按钮后，弹窗的状态
 */
export interface IButtonState {
  current: {
    /**
     * 当前弹出的窗口对应的模块ID
     */
    moduleId: idType,
    /**
     * 打开的窗口的 moduleId 的历史路径
     */
    parentIdPath: Array<idType>

  }
}