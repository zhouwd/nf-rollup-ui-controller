
// 查询子控件的属性 
import type { IFormItemProps } from './20-form-item'

// 查询方式
import type { EFindKind } from './enum'

// 查询控件 ===========================================================

type idType = string | number

/**
 * 自定义查询方式
 */
export interface ICusFind {
  /**
   * 查询方案编号
   */
  id: idType,
  /**
   * 方案名称
   */
  label: string,
  /**
   * 需要的查询字段ID
   */
  colIds: Array<idType>
}

type queryT = string | number | boolean | any
/**
 * 紧凑型查询：IFindArray
 * * { 字段名:[查询方式, 查询值] } 
 * * { 字段名:[查询方式, [开始,结束] }
 * * { 字段名:[查询方式, [集合] }
 */
export interface IQueryMini {
  [colName: idType]: [
    EFindKind,
    queryT | 
    Array<queryT> |
    [queryT , queryT]
  ]
}

/**
 * 标准型查询：[{colName: '字段名', findKind: 101, value: 123}] IFindObject
 */
export interface IQuery {
  /**
   * 字段名称/字段编号
   */
  colName: idType,
  /**
   * 查询方式
   */
  findKind: EFindKind,
  /**
   * 查询值，数字、字符串、日期，多条件用数组
   */
  value?: idType | boolean | Array<string | number | boolean>
  /**
   * 范围查询的开始的值，number | string | boolean
   */
  valueStart?: idType | boolean,
  /**
   * 范围查询的结束的，number | string | boolean
   */
  valueEnd?: idType | boolean
}

/**
 * 查询控件的属性
 */
export interface IFindProps {
  findMeta: {
    moduleId: number | string, // 模块ID
    quickFind: Array<idType> , // 快速查询的字段
    allFind: Array<idType> , // 全部查询的字段
    customer: Array<ICusFind> , // 自定义的查询方案
    customerDefault: idType, // 默认的查询方案
    reload: boolean, // 是否重新加载配置，需要来回取反
  },
  queryMini: IQueryMini, // 用户输入的查询条件，紧凑型
  querys: Array<IQuery>, // 用户输入的查询条件，标准型
  itemMeta: IFormItemProps, // 表单子控件的属性
  [key: string]: any // el-form 的属性
}

/**
 * 绑定查询表单的 model
 */
export interface IFindModel {
  [key: idType]: {
    /**
     * 字段名称
     */
    colName: string,
    /**
     * 该字段可以使用的查询方式
     */
    findKind: Array<idType>,
    /**
     * 默认查询方式、用户选择的查询方式
     */
    useKind: number,
    /**
     * 该字段可以切换的控件类型，可以没有
     */
    controlKind: Array<idType>,
    /**
     * 默认的控件类型，用户可以切换 controlKind 里面的控件
     */
    useControlKind: idType,
    /**
     * 查询值，数字、字符串、日期，多条件用数组
     */
    value?: idType | boolean | Array<string | number | boolean>
    /**
     * 范围查询的开始的值，number | string | boolean
     */
    valueStart?: idType | boolean,
    /**
     * 范围查询的结束的，number | string | boolean
     */
    valueEnd?: idType | boolean
  }
}

/**
 * 查询需要的状态，便于各个组件传递数据
 */
export interface IFindState {
  /**
   * 是否显示更多的查询条件，对应 dialog
   */
  showMoreFind: boolean,
  /**
   * 绑定查询表单的 model
   */
  findModel: IFindModel,
  /**
   * 用户输入的查询条件，紧凑型
   */
  queryMini: IQueryMini, 
  /**
   * 用户输入的查询条件，标准型
   */
  querys: Array<IQuery>,
  /**
   * 表单子控件的属性
   */
  itemMeta: IFormItemProps, 
  /**
   * 快速查询的字段
   */
  quickFind: Array<idType>,
  /**
   * 临时用于绑定的快速查询的字段
   */
  tmpQuickFind: Array<idType>,
   /**
   * 全部查询的字段
   */
  allFind: Array<idType>,
  /**
   * 自定义的查询方案
   */
  customer: Array<ICusFind>,
  /**
   * 默认的查询方案
   */
  customerDefault: idType
}