import type { PropType } from 'vue'

import type {
  IFromMeta // 表单控件需要的 meta
} from '../types/30-form'

import type { IFormItemList } from '../types/20-form-item'

/**
 * 表单控件需要的属性
 */
export const formProps = {
  /**
   * 表单的完整的 model
   */
  model: {
    type: Object as PropType<any>,
    required: true
  },
  /**
   * 组件联动后，可见组件构成的 model
   */
  partModel: {
    type: Object as PropType<any>,
    default: () => { return {}}
  },
  /**
   * 表单控件的 meta
   */
  formMeta: {
    type: Object as PropType<IFromMeta>,
    default: () => { return {}}
  },
  /**
   * 表单控件的子控件的 meta 集合
   */
  itemMeta: {
    type: Object as PropType<IFormItemList>,
    default: () => { return {}}
  },
  /**
   * 标签的后缀
   */
  labelSuffix: {
    type: String,
    default: '：' 
  },
  /**
   * 标签的宽度
   */
  labelWidth: {
    type: String,
    default: '130px'
  },
  /**
   * 控件的规格
   * * 【element-plus】large / default / small 三选一
   */
  size: {
    type: String as PropType<'large' | 'default'| 'small'>,
    default: 'small'
  }
}