
import { toRaw } from 'vue'

import { Model } from '@naturefw/nf-state'

import type {
  IAnyObject,
  IFormChildMeta,
  IFormChildPropsList
} from '../../main'

import { EControlType } from '../../types/main'

// 动态创建表单的 model IFormChildMetaList
type FormModel = {
  [key: string | number]: any
}
 
// 一个控件对应多字段的时候，拆分字段做默认值
const moreColName = (meta: IFormChildMeta, formModel: FormModel) => {
  const arr = meta.colName.split('_')
  if (arr.length === 1) {
    // 不是多字段
    // formModel[meta.colName] = defVal
  } else {
    // 多字段，拆分设置默认值
    arr.forEach((col: string) => {
      formModel[col] = ''
    })
  }
}

// 控件类型对应的，控件值的类型
const dicType = {
  // 文本
  100: '',
  101: '',
  102: '',
  103: '',
  104: '',
  105: '',
  106: '',
  107: '',
  108: '',
  // 数字
  110: 0,
  111: 0,
  112: 0,
  // 日期
  120: '',
  121: '',
  122: '',
  123: '',
  124: '',
  // 日期范围
  125: [],
  126: [],
  127: [],
  128: [],
  // 时间
  130: '',
  131: [],
  132: '',
  133: [],
  // 上传
  140: '',
  141: '',
  142: '',
  // 选择
  150: false,// 勾选
  151: false, // 开关
  152: [],// 多选组
  153: '',// 单选组
  // 下拉
  160: '',// 下拉单选
  161: [],// 下拉多选
  162: '',// 分组下拉单选
  163: [],// 分组下拉多选
  164: [],// 下拉联动
  165: '',// 树状下拉
  166: []// 树状多选

}

/**
 * 创建表单的 model，传入 input 这类的 meta，依据其中的字段名称和控件类型，字段创建。
 * @param meta 表单子控件的 meta
 * @param colOrder 字段ID，数组。需要哪些字段，以及顺序
 * @param colOrder model的类型：reactive、nf-state 的 Model
 */
export default function createModel<T extends IAnyObject>(
    meta: IFormChildPropsList,
    colOrder: Array<number | string>,
    type: 'Model' | 'reactive' = 'Model'
  ) {
    // 定义一个 model
    const formModel = {} as IAnyObject

    // 依据 meta，创建 model
    colOrder.forEach(key => {
      const _meta: IFormChildMeta = meta[key].meta
      if (_meta.controlType < 200) {
        // 表单内置组件
        formModel[_meta.colName] = dicType[_meta.controlType]
      } else {
        formModel[_meta.colName] = ''
      }

      // 看看有没有设置默认值
      if (typeof _meta.defValue !== 'undefined' ) {
        switch (_meta.defValue) {
          case '':
            break
          case '{}':
            formModel[_meta.colName] = {}
            break
          case '[]':
            formModel[_meta.colName] = []
            break
          case '{{now}}':
            formModel[_meta.colName] = new Date()
            break
          default:
            formModel[_meta.colName] = toRaw(_meta.defValue)
            break
        }
      }

      if (Array.isArray(formModel[_meta.colName] )){
        // 数组类型，有可能需要对应多个字段
        moreColName(_meta , formModel)
      }

    })

    const re = Model<T>(formModel as T)
 
    return re // returnModel<T>(formModel as T)
    
  }
 