
import { reactive } from 'vue'

import type { IFormChildMetaList } from '../../main'

/**
 * 一个字段占据几个 格子
 */
type FormColSpan = {
  [id: number | string]: number
}

// 支持的列数
type IColNumer = 1 | 2 | 3 | 4 | 6 | 8 | 12 | 24

// 列数与 span 的字典
const dicColSpan = {
  1: 24,
  2: 12,
  3: 8,
  4: 6,
  6: 4,
  8: 3,
  12:2,
  24:1
}

/**
 * 单列情况下，合并字段的情况计算span
 */
const getSpanForSinger = (colCount: -6 | -4 | -3 | -2 ) => {

  switch(colCount) {
    case -6:
      return 4
    case -4:
      return 6
    case -3:
      return 8
    case -2:
      return 12
    default:
      console.error(`表单控件不支持【${colCount}】！单列合并请选择 -6、-4、-3、-2`)
      break
  }

} 

/**
 *  处理一个控件占用几个 格子 的需求
 * @param columnsNumber 表单控件的列数，支持1、2、3、4、6、8、12、24列
 * @param itemMeta 表单子控件的 meta
 * @returns 
 */
export const getColSpan = (columnsNumber: IColNumer, itemMeta: IFormChildMetaList ) => {
  // 确定一个组件占用几个格子
  const formColSpan = reactive<FormColSpan>({})

  // 根据配置里面的colCount，设置 formColSpan
  const setFormColSpan = (num = columnsNumber) => {
    // const num = num // 表单的列数
    const moreColSpan = dicColSpan[num] //  24 / num // 一个控件默认占多少格子
    if (!moreColSpan) {
      // 不支持的列数
      console.error(`表单控件不支持【${num}】列！请选择 1、2、3、4、6、8、12、24列`)
      return
    }
    // 表单子控件的属性，用于获取一个控件占几份。

    // 遍历表单子控件属性
    for (const key in itemMeta) {
      const m = itemMeta[key]
      if (typeof m.colCount === 'number') {
        // 设置了一个控件占的份数
        if (num === 1) {
          // 单列
          formColSpan[m.columnId] = (m.colCount >= 1) ? 
            moreColSpan : // 一个控件最大占24格子
            getSpanForSinger(m.colCount as (-6 | -4 | -3 | -2 )) // 多控件占一行， 24/控件占的份数
        } else {
          // 多列
          formColSpan[m.columnId] =  (m.colCount < 1) ?
            moreColSpan : // 多控件占一行的控件视为占一份
            moreColSpan * m.colCount // 格子数 * 份数
        }
        if (formColSpan[m.columnId] > 24) {
          formColSpan[m.columnId] = 24
        }
      } else {
        // 没有设置，默认占一行
        formColSpan[m.columnId] = moreColSpan
      }
    }
  }

  // setFormColSpan()

  return {
    formColSpan,
    setFormColSpan
  }
}