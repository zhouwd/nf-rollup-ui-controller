import { reactive, watch } from 'vue'

// 创建局部 model
import createPartModel from './createPartModel'
 
// 表单控件需要的 meta
import type { IFormChildMetaList, IFromMeta, IAnyObject } from '../../main'

type ShowCol = {
  [id: number | string]: boolean
}
/**
 * 设置备选项和子控件的联动
 * @param formMeta 表单控件的meta
 * @param model 表单完整的 model
 * @param partModel 表单部分 的 model
 * @returns 
 */
export default function setControlShow<T extends IAnyObject>(
  formMeta: IFromMeta,
  itemMeta: IFormChildMetaList,
  model: T,
  partModel: any
) {
  
  const {
    linkageMeta,
    colOrder
  } = formMeta
  
  // 设置字段的是否可见
  const showCol = reactive<ShowCol>({})
  // 依据meta设置，默认都可见
  const setShow = () => {
    if (typeof itemMeta === 'object') {
      for (const key in itemMeta) {
        showCol[key] = true
      }
    }
  }

  // 设置联动
  const setFormColShow = () => {
    setShow()
    // 数据变化，联动组件的显示
    if (typeof linkageMeta === 'object' && Object.keys(linkageMeta).length > 0) {
      for (const key in linkageMeta) {
        // 配置里面设置的主动组件。
        const mainComp = linkageMeta[key]
        // 主动组件的字段名称
        const colName = itemMeta[key].colName
        // 监听组件的值，有变化就重新设置局部 model
        if (typeof model[colName] !== 'undefined') {
          // 监听主动组件的值的变化
          watch(() => model[colName], (modelValue, oldValue) => {
            if (modelValue === null) {
              // 选项被清空了，设为全部可见
              Object.keys(itemMeta).forEach(key => {
                showCol[key] = true
              })
              return
            } 

            if (typeof mainComp[modelValue] === 'undefined') {
              // 配置信息里面，没有设置选项值 需要显示的字段。
              return
            }

            switch (typeof modelValue){
              case 'number':
              case 'string':
                // 单选组的选项，先让字段都不可见，然后依据配置信息设置字段可见。
                Object.keys(itemMeta).forEach(key => {
                  showCol[key] = false
                })
                // 配置信息里对应的字段ID集合
                mainComp[modelValue].forEach(id => {
                  showCol[id] = true
                })
                break;
              case 'boolean':
                // 设置指定的控件是否可见，其他控件不设置
                mainComp[modelValue].forEach((id: any) => {
                  let _id = id
                  let bl = modelValue
                  if (Array.isArray(id)) {
                    _id = id[0]
                    // 设置子控件的值
                    const colName = itemMeta[_id].colName
                    model[colName] = id[1]
                  }
                  // 字符串形式的ID，取反
                  if (typeof _id === 'string' ) {
                    bl = !modelValue
                  }
                  showCol[_id] = bl // 以 父级控件的值为准
                })
                break
            }
           
            // 设置部分的 model
            createPartModel<T>(model, partModel, itemMeta, showCol)
          },
          { immediate: true })
        }
      }
      // 监听完整model的值的变化，同步值
      if (typeof partModel !== 'undefined') {
        watch(model, () => {
          for (const key in model) {
            if (typeof(partModel[key]) !== 'undefined') {
              partModel[key] = model[key]
            }
          }
        })
      }
    }
  }
  setFormColShow()
  watch(() => colOrder, (order) => {
    order.forEach((id: string | number) => {
      showCol[id] = true
    })
    // setFormColShow()
  })
  
  return {
    showCol,
    setFormColShow
  }
}