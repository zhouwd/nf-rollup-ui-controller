import type { IFormItemList } from '../../types/20-form-item';
/**
 * @function 创建表单的 v-model
 * @description 依据表单的 meta，自动创建 model
 * @param meta 表单子控件的meta
 * @param colOrder 需要哪些字段，以及顺序
 */
export declare const createModel: (meta: IFormItemList, colOrder: Array<number | string>) => {
    [x: string]: any;
    [x: number]: any;
};
