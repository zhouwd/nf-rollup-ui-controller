import type { IFormItemList } from '../../types/20-form-item';
/**
 * 创建新的局部 model
 * @param model 表单完整的 model
 * @param partModel 表单部分 的 model
 * @param itemMeta 表单子控件的meta
 * @param showCol
 */
export declare const createPartModel: (model: any, partModel: any, itemMeta: IFormItemList, showCol: any) => void;
