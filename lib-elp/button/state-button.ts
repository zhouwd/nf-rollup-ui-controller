
// 操作按钮相关的状态

/**
 * 当前激活的按钮
 * 当前激活的窗口 moduleID、
 * 打开窗口的级层（路径）
 */

import type { InjectionKey } from 'vue'
import { watch } from 'vue'
// 状态
import { defineStore, useStoreLocal } from '@naturefw/nf-state'
import type { IState } from '@naturefw/nf-state/dist/type'

// 类型
import type {
  IFindState,
  IFindProps
} from '../../map'

// 状态的标识，避免命名冲突
const flag = Symbol('button-control') as InjectionKey<string>

/**
 * 注册局部状态
 * * 部门信息管理用的状态 : IMetaDataState & IState
 * @param props 查询控件的 props
 * @returns 
 */
export function regFindState(props: IFindProps): IFindState & IState {
  // 定义 角色用的状态
  const state = defineStore<IFindState>(flag, {
    state: (): IFindState => {
      return {
       
        customerDefault: props.findMeta.customerDefault // 默认的查询方案
      }
    },
    getters: {
    },
    actions: {
      
    }
  },
  { isLocal: true }
  )

  // 创建表单查询表单的 model
  state.queryMini = props.queryMini
  state.querys = props.querys
  // 设置快捷查询
  state.tmpQuickFind.push(...state.quickFind)
  // 记录查询方案
  state.customer = Array.isArray(props.findMeta.customer) ? props.findMeta.customer : []
 
  return state
}

/**
 * 子组件获取状态
 */
export const getFindState = (): IFindState & IState => {
  return useStoreLocal<IFindState & IState>(flag)
}
