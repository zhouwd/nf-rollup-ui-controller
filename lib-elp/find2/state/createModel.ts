
import { reactive } from 'vue'
// 类型
import type {
  IFindState,
  IFindModel
} from '../../map'

/**
 * @function 创建表单的 v-model
 * @description 依据表单的 itemMeta，自动创建 model
 * @param state 表单子控件的meta
 */
export default function createFindModel (state: IFindState): IFindModel {
  const findModel: IFindModel = reactive({})
  const { itemMeta } = state

  // 依据 meta，创建 model
  for(const key in itemMeta) {
    const m = itemMeta[key].formItemMeta
    findModel[m.columnId] = {
      colName: m.colName,
      dateKind: 0, // 查询的时候，使用的日期类型。0：不是日期类型
      findKind: [], // 该字段可以使用的查询方式
      useKind: 21, // 默认查询方式、用户选择的查询方式
      controlKind: [], // 该字段可以切换的控件类型，可以没有
      useControlKind: '', // 默认的控件类型，用户可以切换 controlKind 里面的控件
      value: '', // 查询值
      valueStart: '', // 范围查询的开始的值
      valueEnd: '' // 范围查询的结束值
    }

    // 根据控件类型设置属性值
    switch (m.controlType) {
      case 102: // 密码
        findModel[m.columnId].findKind = []
        findModel[m.columnId].value = ''
        break
      case 100: // 多行
      case 101: // 单行
      case 103: // 
      case 104:
      case 105: // URL
      case 106:
        findModel[m.columnId].findKind = [21, 22, 23, 24, 25, 26, 27, 28]
        findModel[m.columnId].value = ''
        break
      case 107: // 选填
        findModel[m.columnId].findKind = [21, 22]
        findModel[m.columnId].value = ''
        break
      case 108: // 颜色
        findModel[m.columnId].findKind = [21, 23]
        findModel[m.columnId].value = ''
        break
      case 140: // 上传
      case 141:
      case 142:
        findModel[m.columnId].findKind = [21, 23, 25, 26]
        findModel[m.columnId].value = ''
        break
      case 110: // 数字
      case 111:
      case 112:
        findModel[m.columnId].findKind = [21, 17, 13, 14, 15, 16, 22, 18, 19]
        findModel[m.columnId].value = null
        findModel[m.columnId].valueStart = null
        findModel[m.columnId].valueEnd = null
        break
      case 120: // 日期
      case 121: // 日期时间
      case 122: // 年月
      case 123: // 年周
      case 124: // 年
        findModel[m.columnId].findKind = [21, 17, 13, 14, 15, 16, 18, 19]
        findModel[m.columnId].value = ''
        findModel[m.columnId].controlKind = ['日期','年月','年']

        break
      case 125: // 日期范围
      case 126: // 日期时间范围
      case 127: // 年月范围
        findModel[m.columnId].findKind = [21, 17, 13, 14, 15, 16, 18, 19]
        findModel[m.columnId].value = []
        break
      case 128: // 多个日期
        findModel[m.columnId].findKind = [21, 41]
        findModel[m.columnId].value = []
        break
      case 130: // 时间
      case 131:
      case 132:
      case 133:
        findModel[m.columnId].findKind = [21, 17, 13, 14, 15, 16, 18, 19]
        findModel[m.columnId].value = ''
        break
      case 150: // 勾选
      case 151: // 开关
        findModel[m.columnId].findKind = [21]
        findModel[m.columnId].value = null
        break
      case 152: // 多选组
      case 161: // 下拉多选
        findModel[m.columnId].findKind = [21, 41, 42]
        findModel[m.columnId].value = []
        findModel[m.columnId].controlKind = ['多选', '下拉多选']
        break
      case 153: // 单选组
      case 160: // 下拉单选
        findModel[m.columnId].findKind = [21, 22, 41, 42]
        findModel[m.columnId].value = null
        findModel[m.columnId].controlKind = ['多选','单选','下拉']
        break
      case 162: // 分组下拉单选
        findModel[m.columnId].findKind = [21, 22]
        findModel[m.columnId].value = null
        break
      case 164: // 下拉联动
        findModel[m.columnId].findKind = [21, 41, 42]
        findModel[m.columnId].value = []
        break
     
      case 165: // 树状下拉
        findModel[m.columnId].findKind = [21, 22]
        findModel[m.columnId].value = []
        break
      case 163: // 分组下拉多选
      case 166: // 树状多选
        findModel[m.columnId].findKind = [21, 41, 42]
        findModel[m.columnId].value = []
        break
    }
  }

  return findModel
}
 