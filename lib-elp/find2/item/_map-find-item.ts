// 表单的子控件和查询的子控件的对应关系
import { AllFormItem } from '../../main'

export const findItemDict = {
  // 文本类
  100: AllFormItem.nfText, //'nf-text',
  101: AllFormItem.nfText, //'nf-text',
  102: AllFormItem.nfText, //'nf-text',
  103: AllFormItem.nfText, //'nf-text',
  104: AllFormItem.nfText, //'nf-text',
  105: AllFormItem.nfText, //'nf-text',
  106: AllFormItem.nfText, //'nf-text',
  107: AllFormItem.nfAutocomplete, //'nf-text',
  108: AllFormItem.nfText, //'nf-text',
  // 数字
  110: AllFormItem.nfNumber, // 'nf-number',
  111: AllFormItem.nfNumber, // 'nf-number',
  112: AllFormItem.nfNumber, // 'nf-number',
  // 日期
  120: AllFormItem.nfDate, // 'nf-date',
  121: AllFormItem.nfDatetime, // 'nf-date',
  122: AllFormItem.nfMonth, // 'nf-date',
  123: AllFormItem.nfWeek,
  124: AllFormItem.nfYear, // 'nf-date', // 'nf-year',
  125: AllFormItem.nfDate, // 'nf-date', // 'nf-date-range',
  126: AllFormItem.nfDatetime, // 'nf-date', // 'nf-datetime-range',
  127: AllFormItem.nfMonth, // 'nf-date', // 'nf-month-range',
  128: AllFormItem.nfDates, // 'nf-date', // 'nf-dates',
  // 时间
  130: AllFormItem.nfTimePicker,
  131: AllFormItem.nfTimePicker,
  132: AllFormItem.nfTimeSelect,
  133: AllFormItem.nfTimeSelect,
  // 上传，文件、图片 140
  140: AllFormItem.nfText, //'nf-text',
  141: AllFormItem.nfText, //'nf-text',
  142: AllFormItem.nfText, //'nf-text',
  // 平铺选择
  150: AllFormItem.nfCheckbox,
  151: AllFormItem.nfSwitch,
  152: AllFormItem.nfCheckboxs,
  153: AllFormItem.nfRadios,
  // 下拉选项
  160: AllFormItem.nfSelect, // 'nf-select',
  161: AllFormItem.nfSelect, // 'nf-select',
  162: AllFormItem.nfSelectGroup,
  163: AllFormItem.nfSelectGroup,
  164: AllFormItem.nfSelectCascader,
  165: AllFormItem.nfSelectTree,
  166: AllFormItem.nfSelectTree
}